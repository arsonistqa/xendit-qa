exports.config = {

    directConnect: true,
    baseUrl: 'https://www.online-calculator.com/full-screen-calculator/',
    capabilities: {
        'browserName': 'chrome'
    },

    getPageTimeout: 60000,
    allScriptsTimeout: 60000,
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    suites: {
        all: ['features/calculator.feature']
    },
    cucumberOpts: {
        require: [
            'step-definitions/calculator-steps.js'
        ],
    },

    onPrepare: async function () {
        const {Given, Then, When, Before} = require('cucumber');
        global.Given = Given;
        global.When = When;
        global.Then = Then;
        global.Before = Before;

        browser.waitForAngularEnabled(false);
    },
};
