Feature: Online calculator

  Scenario Outline: Test online calculator functions
    Given Go to application page
    When I enter following values and press EC button
      | value1   | value2   | operator   |
      | <value1> | <value2> | <operator> |
    Then I should be able to see result
      | expected   |
      | <expected> |

    Examples:
      | value1 | value2 | operator | expected  |
      | 1      | 1      | +        | 2         |
      | 0.1    | 0.2    | -        | -0.1      |
      | 999    | 1      | +        | 1000      |
      | 4      | 2      | /        | 2         |
      | -0.1   | 0.2    | /        | -0.5      |
