const expect = require('chai').expect;
const PNG = require('pngjs').PNG;
const pixelmatch = require('pixelmatch');
const fs = require("fs");

var webdriver = require('selenium-webdriver'),
    By = webdriver.By;
var imagePath, expectedImgPath, now;

module.exports = function () {

    this.Given(/^Go to application page$/, function () {
        browser.get(browser.baseUrl);
    });

    this.When(/^I enter following values and press EC button$/, function (dataTable) {
        var strValue1 = dataTable.raw()[1][0];
        var strValue2 = dataTable.raw()[1][1];
        var strOperator = dataTable.raw()[1][2];

        enterValue(strValue1);
        enterValue(strOperator);
        enterValue(strValue2);
        enterValue('=');

        now = new Date().getTime();
        imagePath = "reports/screenshots/" + now + ".png";
        takeScreenshot(imagePath);
    });

    this.Then(/^I should be able to see result$/, function (table) {
        var numDiffPixels;
        expectedImgPath = 'data/_' + table.raw()[1][0] + '.tiff';

        if (!fs.existsSync(expectedImgPath)) {
            browser.refresh();
            enterValue(table.raw()[1][0]);
            enterValue('=');
            takeScreenshot(expectedImgPath);
        }

        return verify_calculatedAmount(imagePath, expectedImgPath).then(function (value) {
            numDiffPixels = value;
            expect(numDiffPixels).to.equal(0);
        });
    });

    function enterValue(value) {
        value.split('').forEach(function (symbol) {
            var resultSymbol = '';
            switch (symbol) {
                case '0':
                    resultSymbol = protractor.Key.NUMPAD0;
                    break;
                case '1':
                    resultSymbol = protractor.Key.NUMPAD1;
                    break;
                case '2':
                    resultSymbol = protractor.Key.NUMPAD2;
                    break;
                case '3':
                    resultSymbol = protractor.Key.NUMPAD3;
                    break;
                case '4':
                    resultSymbol = protractor.Key.NUMPAD4;
                    break;
                case '5':
                    resultSymbol = protractor.Key.NUMPAD5;
                    break;
                case '6':
                    resultSymbol = protractor.Key.NUMPAD6;
                    break;
                case '7':
                    resultSymbol = protractor.Key.NUMPAD7;
                    break;
                case '8':
                    resultSymbol = protractor.Key.NUMPAD8;
                    break;
                case '9':
                    resultSymbol = protractor.Key.NUMPAD9;
                    break;
                case '*':
                    resultSymbol = protractor.Key.MULTIPLY;
                    break;
                case '+':
                    resultSymbol = protractor.Key.ADD;
                    break;
                case '-':
                    resultSymbol = protractor.Key.SUBTRACT;
                    break;
                case '.':
                    resultSymbol = protractor.Key.DECIMAL;
                    break;
                case '/':
                    resultSymbol = protractor.Key.DIVIDE;
                    break;
                case '=':
                    resultSymbol = protractor.Key.EQUALS;
                    break;
            }
            browser.actions().sendKeys(resultSymbol).perform();
        });
    }

    function takeScreenshot(imagePath) {
        const driver = browser.driver;
        iframe = driver.findElement(By.id("fullframe"));
        var base64Data = "";
        var location = {};
        var bulk = [];

        browser.switchTo().frame(iframe).then(_ => {
            webElement = driver.findElement(By.id("canvas"));
        }).then(_ => {
            webElement.getLocation().then(e => {
                location.x = e.x;
                location.y = e.y;
            });
            webElement.getSize().then(e => {
                location.height = e.height;
                location.width = e.width;
            });
            driver.manage().window().getSize().then(e => {
                location.browserHeight = e.height;
                location.browserWidth = e.width;
            });
        }).then(_ => {
            driver.takeScreenshot().then(data => {
                base64Data = data.replace(/^data:image\/png;base64,/, "");
            });
        }).then(_ => {
            const sizeLimit = 700000; // around 700kb
            const imgSize = base64Data.length;
            driver.executeScript(() => {
                window.temp = new Array;
            }).then(_ => {
                for (var i = 0; i < imgSize; i += sizeLimit) {
                    bulk.push(base64Data.substring(i, i + sizeLimit));
                }
                bulk.forEach((element, index) => {
                    driver.executeScript(() => {
                        window.temp[arguments[0]] = arguments[1];
                    }, index, element);
                });
            });
        }).then(_ => {
            driver.executeScript(() => {
                var tempBase64 = window.temp.join("");
                var image = new Image();
                var location = arguments[0];
                image.src = "data:image/png;base64," + tempBase64;
                image.onload = function () {
                    var canvas = document.createElement("canvas");
                    canvas.height = location.height;
                    canvas.width = location.width;
                    canvas.style.height = location.height + 'px';
                    canvas.style.width = location.width + 'px';
                    var ctx = canvas.getContext('2d');
                    ctx.drawImage(image, -location.x, -location.y);
                    window.canvasData = canvas.toDataURL();
                    window.temp = [];
                }
            }, location);
        }).then(_ => {
            return driver.executeScript(() => {
                var data = window.canvasData;
                window.canvasData = "";
                return data;
            }).then(data => {
                var tempData = data.replace(/^data:image\/png;base64,/, "");
                fs.writeFileSync(imagePath, tempData, "base64");
            }).then(_ => {
                browser.switchTo().defaultContent();
            });
        });
    }

    function verify_calculatedAmount(localFilePath, dataFilePath) {
        var img1, pathDiffImag, numDiffPixels;

        return browser.driver.wait(function () {
            return fs.existsSync(localFilePath);
        }, 5000).then(_ => {
            img1 = PNG.sync.read(fs.readFileSync(localFilePath));
        }).then(_ => {
            const img2 = PNG.sync.read(fs.readFileSync(dataFilePath));
            const {width, height} = img1;
            const diff = new PNG({width, height});
            numDiffPixels = pixelmatch(img1.data, img2.data, diff.data, width, height, {threshold: 0.5});
            if (numDiffPixels > 0) {
                pathDiffImag = 'reports/diff/diff_' + now + '.png';
                fs.writeFileSync(pathDiffImag, PNG.sync.write(diff));
            }
            return numDiffPixels;
        })
    }
};
